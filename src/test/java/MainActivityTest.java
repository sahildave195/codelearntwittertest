package test.java;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.robolectric.Robolectric.shadowOf;

import org.codelearn.twitter.R;
import org.codelearn.twitter.TweetDetailActivity;
import org.codelearn.twitter.models.Tweet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowTextView;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

	private NotificationManager notificationManager;
	private Notification notification1 = new Notification();
	private PendingIntent pIntent;
	private TweetDetailActivity detailAct;

	@SuppressWarnings("deprecation")
	@Before
	public void setup() throws CanceledException {
		notificationManager = (NotificationManager) Robolectric.application
				.getSystemService(Context.NOTIFICATION_SERVICE);

		String notiTitle = "codelearn mentioned you";
		String notiText = "you are notified";

		Tweet notiTweet = new Tweet();
		notiTweet.setTitle(notiTitle);
		notiTweet.setBody(notiText);

		Intent intent = new Intent(
				Robolectric.application.getApplicationContext(),
				TweetDetailActivity.class);
		intent.putExtra("MyClass", notiTweet);

		int requestID = (int) System.currentTimeMillis();
		int flags = PendingIntent.FLAG_CANCEL_CURRENT;
		pIntent = PendingIntent.getActivity(
				Robolectric.application.getApplicationContext(), requestID,
				intent, flags);

		notification1.setLatestEventInfo(
				Robolectric.application.getApplicationContext(), "title",
				"text", pIntent);

		detailAct = new TweetDetailActivity();

		// Intent is sent from here
		pIntent.send(detailAct, 1, intent);

	}

	@Test
	public void testNotify() throws Exception {

		// create a notification
		notificationManager.notify(1, notification1);

		// check the size of the notificationmanager should be 1
		// because there is only 1 notification
		assertEquals(1, shadowOf(notificationManager).size());

		// get the notification by using the notification id (here 1)
		// and check if it is notification1
		assertEquals(notification1, shadowOf(notificationManager)
				.getNotification(null, 1));

		Intent detailIntent = shadowOf(detailAct).peekNextStartedActivity();
		System.out.println(detailIntent.toString());
		// ^
		// Intent{componentName=ComponentInfo{org.codelearn.twitter/org.codelearn.twitter.TweetDetailActivity},
		// extras=Bundle[{MyClass=org.codelearn.twitter.models.Tweet@37264b74}]}

		assertThat(detailIntent, notNullValue());
		assertTrue("The next activity is not TweetDetailActivity",
				Robolectric.shadowOf(detailIntent).getComponent()
						.getClassName() == TweetDetailActivity.class
						.getCanonicalName());

		assertThat(((Tweet) detailIntent.getSerializableExtra("MyClass"))
				.getTitle().toString(), equalTo("codelearn mentioned you"));
		assertThat(((Tweet) detailIntent.getSerializableExtra("MyClass"))
				.getBody().toString(), equalTo("you are notified"));

		// Next - Check validity of activity and components
		Activity detailAct = Robolectric
				.buildActivity(TweetDetailActivity.class)
				.withIntent(detailIntent).create().get();

		assertNotNull("detailActivity is null ", detailAct);

		TextView titleText = (TextView) detailAct.findViewById(R.id.tweetTitle);
		ShadowTextView shadowTitle = Robolectric.shadowOf(titleText);
		assertTrue("The title in your tweet is not correct.", shadowTitle
				.innerText().toString().contains("codelearn"));
	}

	@Test
	public void testCancel() throws Exception {
		notificationManager.notify(1, notification1);
		notificationManager.cancel(1);

		assertEquals(0, shadowOf(notificationManager).size());
		assertNull(shadowOf(notificationManager).getNotification(null, 1));
	}
}